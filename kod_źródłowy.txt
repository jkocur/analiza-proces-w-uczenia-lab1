a <- 2*log(5)
b <- 3*a
max(a,b)
#c
v <- c(25:45)
median(v)
#d
median()
??median()
#e
setwd("E:\Nowy folder (3)\R studio")
setwd("E:/Nowy folder (3)/R studio")
a <- "macbooki apple"
write(a, file='pkt_e.csv')
rm(a)
a
a <- read.csv(file = 'pkt_e.csv', sep = ',')
a
#f
install.packages("gridExtra")
library("gridExtra")
g <- tableGrob(Orange)
grid.arrange(g)
#g
c <- seq(200, 130, -5)
#h)
a <- c(19:5)
b <- c(11:23)
d <- c(b, a)
d
#i
nazwa <- c('Air', 'Pro','Retina',
+            'Air 2', 'Pro 2', 'Pro Air',
+            'Pro 3', 'Pro 4', 'Air 3 ',
+            'Retina 2')
nazwa <- c('Air', 'Pro','Retina', 'Air 2', 'Pro 2', 'Pro Air', 'Pro 3', 'Pro 4', 'Air 3 ','Retina 2')
ekran <- c('13,3 "', '13,3 "', '15,2 "', '13,3 "', '16 "', '15,4 "', '13,3 "', '15,4 "', '16 "', ' 13,3 "')
pamiec_RAM <- c('8 GB', '8 GB', '8 GB ', '8 GB ', '16 GB', '4 GB', '8 GB', '16 GB', '16 GB', ' 8 GB')
dysk <- c('128GB SSD', '128GB SSD', '256GB SSD ', '512GB SSD ', '256GB SSD', '256GB SSD', '128GB SSD', '512GB SSD', '256GB SSD', ' 512GB SSD')
cena <- c('2599', '2400', '1999 ', '2199 ', '3199', '3099', '2699', '3599', '3099', ' 3099')
liczb_opinii <- c('9', '20', '199 ', '219 ', '39', '99', '26', '359', '99', ' 30')
Macbooki <- data.frame(nazwa, ekran, pamiec_RAM, dysk, cena, liczb_opinii)
mean(Macbooki$cena)
mean(Macbooki, cena)
mean(cena)
mean(Macbooki.cena)
x <- mean(Macbooki.cena)
x <- mean(cena)
x <- mean(Macbooki$cena)
x <- mean(Macbooki, cena)
xx = Macbooki$cena
mean(xx)
cena <- c(2599, 2400, 1999 , 2199 , 3199, 3099, 2699, 3599, 3099,  3099)
liczb_opinii <- c(9, 20, 199 , 219 , 39, 99, 26, 359, 99, 30)
Macbooki <- data.frame(nazwa, ekran, pamiec_RAM, dysk, cena, liczb_opinii)
mean(Macbooki$cena)
#j
newRow <- data.frame(nazwa = 'Pro Retina', ekran = '30 "', pamiec_RAM = '8 GB', dysk = '256GB SSD', cena = 3099, liczb_opinii = 19)
Macbooki <- rbind(Macbooki, newRow)
mean(Macbooki$cena)
#k
Macbooki$ocena <- c('5', '5', '3', '5', '4', '5', '5', '5', '3', '2', '4')
aggregate(Macbooki$cena, list(Macbooki$ocena), mean)
#l
newRow <- data.frame(nazwa = 'Pro Mini', ekran = '25 "', pamiec_RAM = '16 GB', dysk = '256GB SSD', cena = 3299, liczb_opinii = 12, ocena = 5)
Macbooki <- rbind(Macbooki, newRow)
newRow <- data.frame(nazwa = 'Pro Mini 2', ekran = '15 "', pamiec_RAM = '16 GB', dysk = '256GB SSD', cena = 3099, liczb_opinii = 12, ocena = 4)
Macbooki <- rbind(Macbooki, newRow)
newRow <- data.frame(nazwa = 'Pro Mac', ekran = '15 "', pamiec_RAM = '8 GB', dysk = '256GB SSD', cena = 2799, liczb_opinii = 12, ocena = 5)
Macbooki <- rbind(Macbooki, newRow)
newRow <- data.frame(nazwa = 'Pro 9', ekran = '15 "', pamiec_RAM = '16 GB', dysk = '512GB SSD', cena = 4799, liczb_opinii = 52, ocena = 5)
Macbooki <- rbind(Macbooki, newRow)
dane <- aggregate(Macbooki$liczb_opinii, list(Macbooki$ocena), sum)
barplot(dane[,2], names.arg = dane[,1], main = 'Liczba reprezentantow kazdej z ocen')
#m
labels <- round(dane[,2]/sum(dane[,2]) * 100, 1)
labels <- paste(labels, "%", sep="")
pie(dane[,2], radius = 1, col = rainbow(length(dane[,2])), labels = labels)
legend(1.6, 0.8, dane[,1], cex=0.8, fill=rainbow(length(dane[,2])))
#m wachlarz
install.packages("plotrix")
library(plotrix)
percentage <- table(Macbooki$ocena) / length(Macbooki$ocena)
fan.plot(percentage, labels = names(percentage), main = "Procentowy udzial oceny")
#n
new_column <- ifelse(Macbooki$liczb_opinii>100,'wiecej 100 opinii', ifelse(Macbooki$liczb_opinii>=50, '50-100 opinii', ifelse(Macbooki$liczb_opinii>0, 'mniej 50 opinii', 'nie ma')))
Macbooki['status_opinii'] <- factor(new_column)
pie(table(Macbooki$status_opinii), radius = 1, col = rainbow(length(Macbooki$status_opinii)))
#o
for (i in 1:length(Macbooki$nazwa)){ print(paste(Macbooki$nazwa[i], 'posiadaocene klientow', Macbooki$ocena[i], 'bo posiada liczbe opinii', Macbooki$liczb_opinii[i]))}
for (i in 1:length(Macbooki$nazwa)){ print(paste(Macbooki$nazwa[i], 'posiada ocene klientow', Macbooki$ocena[i], 'bo posiada liczbe opinii', Macbooki$liczb_opinii[i]))}
#p
write.csv(Macbooki, 'Macbooki.csv')
dane <- read.csv('Macbooki.csv')